# LDAP SERVER
## @isx43567395
### Servidor LDAP

Repositorio ldap 2020

Podéis encontrar las imagenes docker al dockerhub [zeuslawl] (https://hub.docker.com/u/zeuslawl/)

ASIX M06-ASO Escola del treball de barcelona

Imagen:

**isx43567395/ldap20_group** Imagen usuarios identificados por uid.
Por ejemplo: uid=pere,ou=usuaris,dc=edt,dc=org.

Detach:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d zeuslawl/tls20:ldaps

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -d zeuslawl/tls20:ldaps

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -p 636:636 -d zeuslawl/tls20:ldaps
