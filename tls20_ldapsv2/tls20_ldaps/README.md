# M06-ASO LDAP SERVER BASE
## Escola Del Treball
### 2HISX 2020-2021
### Alejandro Lopez

Servidor ldapserver amb base de dades d'usuaris per UID i amb grups posix ben configurats.

**Detach**

`$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d zeuslawl/tls20:ldapsv2`

**Interactiu**

`$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it zeuslawl/tls20:ldapsv2 /bin/bash`
